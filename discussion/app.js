// Express server setup
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use all the routes defined in the "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js');
const app = express();

const port = 4001;
// Middlewares:
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb://yobynnam:admin1234@ac-za4ipyn-shard-00-00.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-01.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-02.ufiwlyz.mongodb.net:27017/s36?ssl=true&replicaSet=atlas-x79x6p-shard-0&authSource=admin&retryWrites=true&w=majority`, {
/*
mongoose.connect(`mongodb+srv://yobynnam:admin1234@zuittbootcamp-b197pt.ufiwlyz.mongodb.net/s36?retryWrites=true&w=majority`, */
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));


// http://localhost:4001/tasks/(URI)
// Assigns an endpoint for every database collections
app.use('/tasks', taskRoutes);
// app.use(express.json());
// app.use(express.urlencoded({extended: true}));


app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
});