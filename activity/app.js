// Express setup
const express = require('express');

const mongoose = require('mongoose');

// taskRoute.js
const taskRoutes = require('./routes/taskRoutes.js');

const app = express();

// port
const port = 4001;

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/tasks', taskRoutes);

// mongoose connection
mongoose.connect(`mongodb://yobynnam:admin1234@ac-za4ipyn-shard-00-00.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-01.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-02.ufiwlyz.mongodb.net:27017/s36?ssl=true&replicaSet=atlas-x79x6p-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));

app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
});